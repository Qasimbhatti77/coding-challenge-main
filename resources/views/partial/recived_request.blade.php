@php
    use App\Models\User;
    $receivedRequests = User::whereHas('receivedRequests', function ($query) {
        $query
            ->where('user_id', Auth::user()->id)
            ->whereNotNull('receive_request_id')
            ->whereNull('sent_request_id');
    })->get();
@endphp
@foreach ($receivedRequests as $user)

    <div class="my-2 shadow text-white bg-dark p-1">
        <div class="d-flex justify-content-between">
            <table class="ms-1">
                <td class="align-middle">{{ $user->name }}</td>
                <td class="align-middle"> - </td>
                <td class="align-middle">{{ $user->email }}</td>
                <td class="align-middle">
            </table>
            <div>
                @foreach ($user->receivedRequests as $request)
                @php
                    $requestID=$request->id;

                @endphp
            @endforeach

                <button id="accept_request" class="btn btn-success me-1" data-requestID="{{ $requestID }}" data-freind_id="{{ $user->id }}"
                    data-user_id="{{ Auth::user()->id }}">Accept Request</button>
            </div>
        </div>
    </div>
@endforeach
