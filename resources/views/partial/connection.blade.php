@php
    use App\Models\User;
    $user=User::find(Auth::user()->id);
@endphp

@foreach ($user->friends as $friend)
    <div class="my-2 shadow text-white bg-dark p-1">
        <div class="d-flex justify-content-between">
            <table class="ms-1">
                <td class="align-middle">{{ $friend->name }}</td>
                <td class="align-middle"> - </td>
                <td class="align-middle">{{ $friend->email }}</td>
                <td class="align-middle">
            </table>
            <div>
                <button id="none" class="btn btn-danger me-1"
                    data-id="{{ $friend->id }}">Withdraw Request</button>
            </div>
        </div>
    </div>
@endforeach

