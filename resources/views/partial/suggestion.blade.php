@php
    use App\Models\User;
    // $suggestions = User::where('id', '!=', Auth::id())->get();
    $usersWithoutRequests = User::whereDoesntHave('sendRequests')
    ->where('id', '!=', Auth::id())
    ->get();
@endphp

{{-- Testing --}}
@php
    $user = User::find(Auth::user()->id); // Replace 1 with the user ID you want to retrieve the records for


@endphp
@foreach ($usersWithoutRequests as $user)
    <div class="my-2 shadow text-white bg-dark p-1">
        <div class="d-flex justify-content-between">
            <table class="ms-1">
                <td class="align-middle">{{ $user->name }}</td>
                <td class="align-middle"> - </td>
                <td class="align-middle">{{ $user->email }}</td>
                <td class="align-middle">
            </table>
            <div>
                <button id="create_request_btn_" class="btn btn-primary me-1"
                    data-id="{{ $user->id }}">Connect</button>
            </div>
        </div>
    </div>
@endforeach
