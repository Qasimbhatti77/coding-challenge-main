@php
    use App\Models\User;
    // $suggestions = User::where('id', '!=', Auth::id())->get();
    $sendRequest = User::whereHas('sendRequests', function ($query) {
    $query->where('user_id', Auth::id())->whereNotNull('sent_request_id');
})->get();

@endphp
@foreach ($sendRequest as $user)
    <div class="my-2 shadow text-white bg-dark p-1">
        <div class="d-flex justify-content-between">
            <table class="ms-1">
                <td class="align-middle">{{ $user->name }}</td>
                <td class="align-middle"> - </td>
                <td class="align-middle">{{ $user->email }}</td>
                <td class="align-middle">
            </table>
            <div>
                <button id="with_draw_request" class="btn btn-danger me-1"
                    data-id="{{ $user->id }}">Withdraw Request</button>
            </div>
        </div>
    </div>
@endforeach
