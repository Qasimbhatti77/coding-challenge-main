<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
// Home Controller for Coding Challenge Main
Route::controller(HomeController::class)
        ->group(function () {
            Route::get('/home', 'index');
            Route::get('/get-suggestions', 'get_suggestion');
            Route::get('/get-sent-requests', 'get_sent_request');
            Route::get('/get-received-requests', 'get_received_request');
            Route::get('/get-connections', 'get_connections');

            // Sending Request Route
            Route::post('/connection', 'connect');
            Route::post('/withdraw', 'withdraw');
            Route::post('/accept', 'accept');
            // Sending Request Route

        });
// Home Controller for Coding Challenge Main
