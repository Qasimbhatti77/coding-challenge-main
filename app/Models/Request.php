<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Request extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
    ];


    public function receiver()
    {
        return $this->belongsTo(User::class, 'receive_request_id', 'user_id');
    }

}
