<?php

namespace App\Http\Controllers;

use App\Models\Connection;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Request as UserRequestModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Get suggestions for the logged-in user
        $suggestions = User::where('id', '!=', Auth::id())->get();

        return view('home', ['suggestions' => $suggestions]);
    }

    /**
     * Send a friend request.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function connect(Request $request)
    {
        // For Sending Request
        $sendRequest = new UserRequestModel();
        $sendRequest->sent_request_id = $request->userId;
        $sendRequest->user_id = Auth::user()->id;
        $sendRequest->save();
        // For Sending Request

        // For Receiving Request
        $receivedRequest = new UserRequestModel();
        $receivedRequest->receive_request_id = Auth::user()->id;
        $receivedRequest->user_id = $request->userId;
        $receivedRequest->save();
        $html = (string) view('partial.suggestion');

        // For Receiving Request
        return response()->json([
            'message' => 200,
            'html' => $html,
        ]);
    }

    /**
     * Withdraw a friend request.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function withdraw(Request $request)
    {
        // Delete the friend request from both sender and receiver
        UserRequestModel::where('user_id', Auth::user()->id)
            ->where('sent_request_id', $request->userId)
            ->delete();

        UserRequestModel::where('user_id', $request->userId)
            ->where('receive_request_id', Auth::user()->id)
            ->delete();

        $html = (string) view('partial.send_request');

        return response()->json([
            'message' => 200,
            'html' => $html,
        ]);
    }

    /**
     * Accept a friend request.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function accept(Request $request)
    {
        $requestID = $request->requestID;
        $sender = UserRequestModel::find($requestID);
        $userId = $sender->user_id;
        $receiverRequest = $sender->receive_request_id;

        // Delete the friend request from both sender and receiver
        $sender->delete();
        UserRequestModel::where('user_id', $receiverRequest)
            ->where('sent_request_id', $userId)
            ->delete();

        // Create connections for both users
        $connection1 = new Connection();
        $connection1->friend_id = $request->friendId;
        $connection1->user_id = $request->userId;
        $connection1->status = 'accepted';
        $connection1->save();

        $connection2 = new Connection();
        $connection2->friend_id = $request->userId;
        $connection2->user_id = $request->friendId;
        $connection2->status = 'accepted';
        $connection2->save();

        $html = (string) view('partial.recived_request');

        return response()->json([
            'message' => 200,
            'html' => $html,
        ]);
    }

    /**
     * Get friend suggestions.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_suggestion()
    {
        $html = (string) view('partial.suggestion');
        return response()->json([
            'message' => 200,
            'html' => $html,
        ]);
    }

    /**
     * Get sent friend requests.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_sent_request()
    {
        $html = (string) view('partial.send_request');

        return response()->json([
            'message' => 200,
            'html' => $html,
        ]);
    }

    /**
     * Get received friend requests.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_received_request()
    {
        $html = (string) view('partial.recived_request');

        return response()->json([
            'message' => 200,
            'html' => $html,
        ]);
    }

    /**
     * Get user connections.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_connections()
    {
        $html = (string) view('partial.connection');

        return response()->json([
            'message' => 200,
            'html' => $html,
        ]);
    }
}
