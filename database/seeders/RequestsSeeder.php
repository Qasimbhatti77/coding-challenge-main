<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Request;

class RequestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Request::create([
            'user_id' => 1,
            'sent_request_id' => 2,
            'receive_request_id' => null,
        ]);

        Request::create([
            'user_id' => 2,
            'sent_request_id' => 1,
            'receive_request_id' => null,
        ]);
        Request::create([
            'user_id' => 2,
            'sent_request_id' => null,
            'receive_request_id' => 3,
        ]);

        // Add more requests as needed
    }
}
