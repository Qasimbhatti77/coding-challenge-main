$(document).ready(function() {

    $.ajax({
        url: '/get-suggestions',
        success: function(response) {
            $('#content').html(response.html); // Update the content with the fetched data
        },
        error: function() {
            $('#dynamic-component').html('<p>Error occurred while fetching data.</p>'); // Display error message
        }
    });
    // Function to update content with AJAX
    function updateContentWithAjax(component, url) {
        $('#content').empty().append('<div id="dynamic-component"></div>');

        // Make an AJAX request to fetch the data
        $.ajax({
            url: url,
            success: function(response) {
                $('#dynamic-component').html(response.html); // Update the content with the fetched data
            },
            error: function() {
                $('#dynamic-component').html('<p>Error occurred while fetching data.</p>'); // Display error message
            }
        });
    }

    // Event handler for the "Get Suggestions" button
    $('#get_suggestions_btn').on('click', function() {
        var component = `<x-suggestion :mode="'sent'" />`;
        var url = '/get-suggestions'; // Replace with the actual URL to fetch suggestions

        // Update the content with the "x-suggestion" component and fetch data dynamically
        updateContentWithAjax(component, url);
    });

    // Event handler for the "Get Sent Requests" button
    $('#get_sent_requests_btn').on('click', function() {
        var component = `<x-request :mode="'sent'" />`;
        var url = '/get-sent-requests'; // Replace with the actual URL to fetch sent requests

        // Update the content with the "x-request" component and fetch data dynamically
        updateContentWithAjax(component, url);
    });

    // Event handler for the "Get Received Requests" button
    $('#get_received_requests_btn').on('click', function() {
        var component = `<x-request :mode="'received'" />`;
        var url = '/get-received-requests'; // Replace with the actual URL to fetch received requests

        // Update the content with the "x-request" component and fetch data dynamically
        updateContentWithAjax(component, url);
    });

    // Event handler for the "Get Connections" button
    $('#get_connections_btn').on('click', function() {
        var component = `<x-connection :mode="'sent'" />`;
        var url = '/get-connections'; // Replace with the actual URL to fetch connections

        // Update the content with the "x-connection" component and fetch data dynamically
        updateContentWithAjax(component, url);
    });

    // Event handler for the "Suggestion On Click" button
    $(document).on('click', '#create_request_btn_', function() {
        var userId = $(this).data('id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/connection', // Replace with the actual URL you want to send the request to
            method: 'POST', // HTTP method (e.g., GET, POST, PUT, DELETE)
            data: {
                userId: userId, // Replace with the data you want to send in the request
                // Add more key-value pairs as needed
            },
            success: function(response) {
                // Handle the successful response from the server
                $('#content').empty().html(response.html);
            },
            error: function(xhr, status, error) {
                // Handle any errors that occur during the request
                console.log('An error occurred:', error);
            }
        });
    });

    // Event handler for the "Withdraw Request" button
    $(document).on('click', '#with_draw_request', function() {
        var userId = $(this).data('id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/withdraw', // Replace with the actual URL you want to send the request to
            method: 'POST', // HTTP method (e.g., GET, POST, PUT, DELETE)
            data: {
                userId: userId, // Replace with the data you want to send in the request
                // Add more key-value pairs as needed
            },
            success: function(response) {
                // Handle the successful response from the server
                $('#content').empty().html(response.html);
            },
            error: function(xhr, status, error) {
                // Handle any errors that occur during the request
                console.log('An error occurred:', error);
            }
        });
    });

    // Event handler for the "Accept Request" button
    $(document).on('click', '#accept_request', function() {
        var friendId = $(this).data('freind_id');
        var requestID = $(this).data('requestid');
        var userId = $(this).data('user_id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/accept', // Replace with the actual URL you want to send the request to
            method: 'POST', // HTTP method (e.g., GET, POST, PUT, DELETE)
            data: {
                userId: userId, // Replace with the data you want to send in the request
                requestID: requestID, // Replace with the data you want to send in the request
                friendId: friendId, // Replace with the data you want to send in the request
                // Add more key-value pairs as needed
            },
            success: function(response) {
                // Handle the successful response from the server
                $('#content').empty().html(response.html);
            },
            error: function(xhr, status, error) {
                // Handle any errors that occur during the request
                console.log('An error occurred:', error);
            }
        });
    });
});
